class Component {
    
    constructor() {
        this.state = {
            value: 1
        }
    }

    setState(state) {
        this.state = state;
        this.render();
    }

    h1OnClick() {
        const value = this.state.value + 1;
        this.setState({
            value: value
        })
    }

    render() {
        const value = this.state.value;

        const dDiv = document.createElement('h1');
        dDiv.innerText = `${value}`;
        dDiv.addEventListener('click', this.h1OnClick.bind(this))

        return dDiv;
    }
}

class AppLauncher {

    constructor(rootDomEl) {
        this.rootDomEl = rootDomEl;
    }
    
}